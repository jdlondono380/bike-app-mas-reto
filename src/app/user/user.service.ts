import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Iuser } from './iuser';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public query(): Observable<Iuser[]> {
    return this.http.get<Iuser[]>(`${environment.ENN_POINT}/posts`)
    .pipe(map(res => {
      return res;
    }, error => {
      return console.error('error', error);
    }))
  }

  public getUserByUserId(userId: string): Observable<Iuser>{
    let params = new HttpParams();
    params = params.append('userId', userId);
    console.warn('Params', params);
    return this.http.get<Iuser>(`${environment.ENN_POINT}/posts`,{params: params})
    .pipe(map(res => {
      return res;
    }, error => {
      console.error('error', error)
    }));
  }


}
