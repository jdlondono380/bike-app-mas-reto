import { Component, OnInit } from '@angular/core';
import { IReto } from '../ireto';
import { RetoService } from '../reto.service';

@Component({
  selector: 'app-buscador-list',
  templateUrl: './buscador-list.component.html',
  styleUrls: ['./buscador-list.component.styl']
})
export class BuscadorListComponent implements OnInit {

public buscadorList: IReto[];

  constructor(private buscadorService: RetoService) { }

  ngOnInit() {
    this.buscadorService.query()
    .subscribe(res => {
      console.log('response data', res);
      return this.buscadorList = res;
    }, error => console.error('error')
    );
  }

}
