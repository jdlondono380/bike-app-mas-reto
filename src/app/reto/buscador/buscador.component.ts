import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RetoService } from '../reto.service';
import { IReto } from '../ireto';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.styl']
})
export class BuscadorComponent implements OnInit {

  buscadorFormGroup: FormGroup;
  
  reto: IReto;

  statusSearchPostId: boolean = false;

  formSearchReto = this.formBuilder.group({
    postId: ['']
  });

  constructor(private formBuilder: FormBuilder, private retoService: RetoService ) { }

  ngOnInit() {
  }

  searchPostId(){
    console.warn('Data', this.formSearchReto.value);
    this.retoService.getRetoByPostId(this.formSearchReto.value.postId)
    .subscribe(res => {
      console.warn('Response reto by postId', res);
      this.reto = res;
      this.statusSearchPostId = true;
    }, error => {
      console.warn('No encontrado', error);
      this.reto = null;
      this.statusSearchPostId = false;
    });

  }

}
