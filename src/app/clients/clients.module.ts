import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFormsComponent } from './clients-forms/clients-forms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientsUpdateComponent } from './clients-update/clients-update.component'

@NgModule({
  declarations: [ClientsListComponent, ClientsFormsComponent, ClientsUpdateComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ClientsModule { }
