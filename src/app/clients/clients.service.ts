import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Interface } from './interface';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor( private http: HttpClient ) { }
  

  public query(): Observable <Interface[]> {
    return this.http.get<Interface[]>(`${environment.ENN_POINT}/api/clients`)
    .pipe(map(res => {
      return res;
    }));
  }

  public saveClient(clients: Interface): Observable<Interface> {
    return this.http.post<Interface>(`${ environment.ENN_POINT }/api/clients`, clients)
    .pipe(map(res => {
      return res;
    }));
    }

public getClientsById(id: string){
  return this.http.get<Interface>(`${ environment.ENN_POINT }/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    }));
}

public updateClient(clients: Interface): Observable<Interface>{
  return this.http.put<Interface>(`${ environment.ENN_POINT }/api/clients`,clients)
  .pipe(map(res => {
    return res;
  }));
}

  }

