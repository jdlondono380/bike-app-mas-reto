import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsFormsComponent } from './clients-forms.component';

describe('ClientsFormsComponent', () => {
  let component: ClientsFormsComponent;
  let fixture: ComponentFixture<ClientsFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
