import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { ActivatedRoute, Router, } from '@angular/router';
import { Bike } from '../interfaces/bike';
@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.styl']
})
export class BikesUpdateComponent implements OnInit {

  bikeFormGroup: FormGroup;

  constructor( private formBuilder: FormBuilder, 
    private bikeService:BikesService, 
    private activeteRoute: ActivatedRoute, private router: Router  ) 
    
    { 

    this.bikeFormGroup = this.formBuilder.group({
      id: [''],
      model:  ['', Validators.compose([ Validators.required,Validators.maxLength(20)])],
      price:  ['',Validators.compose([ Validators.required,Validators.maxLength(20)])],
      serial: ['',Validators.compose([ Validators.required,Validators.maxLength(20)])]

    })

   }

  ngOnInit() {

  let id = this.activeteRoute.snapshot.paramMap.get('id');
  console.log('ID Path', id);
  this.bikeService.getBikeById(id)
  .subscribe(res => {
    console.log('get data ok', res);
    this.loadForm(res);
  });

  }

  saveBike(){
    console.log('Datos', this.bikeFormGroup.value);
    this.bikeService.updateBike(this.bikeFormGroup.value)
    .subscribe(res => {
      console.log('Update ok', res);
      this.router.navigate(['/bikes/bikes-list'])
    }, error => {
  console.error('Error', error);
    });
  }

  private loadForm(bike: Bike){
    this.bikeFormGroup.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial
    });
  }
}
